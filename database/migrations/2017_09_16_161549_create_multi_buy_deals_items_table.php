<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiBuyDealsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multi_buy_deals_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('multiBuyDealsId')->unsigned();
            $table->foreign('multiBuyDealsId')->references('id')
            ->on('multi_buy_deals')->onDelete('cascade');

            $table->integer('productId')->unsigned();
            $table->foreign('productId')->references('id')
            ->on('products')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multi_buy_deals_items');
    }
}
