<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductJournalItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_journal_items', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('productJournalId')->unsigned();
            $table->foreign('productJournalId')->references('id')
            ->on('product_journals')->onDelete('cascade');

            $table->integer('productDetailsId')->unsigned();
            $table->foreign('productDetailsId')->references('id')
            ->on('product_details')->onDelete('cascade');

            $table->integer('qty');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_journal_items');
    }
}
