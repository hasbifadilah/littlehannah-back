<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_confirmations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('orderId')->unsigned();
            $table->foreign('orderId')->references('id')
            ->on('orders')->onDelete('cascade');

            $table->datetime('paymentDate');
            $table->decimal('amount');
            $table->string('bank');

            $table->string('transactionType');
            $table->string('accountName');
            $table->string('accountNumber');
            $table->string('image');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_confirmations');
    }
}
