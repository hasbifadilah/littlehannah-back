<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->datetime('orderDate');
            $table->string('paymentMethod');
            $table->decimal('totalPrice');
            $table->decimal('totalDiscount');

            $table->boolean('checkout');
            $table->boolean('paid');
            $table->boolean('cancelled');
            
            $table->integer('voucherItemId')->unsigned()->nullable();
            $table->foreign('voucherItemId')->references('id')
            ->on('voucher_items');

            $table->integer('promotionShippingAreaId')
            ->unsigned()->nullable();
            $table->foreign('promotionShippingAreaId')
            ->references('id')
            ->on('promotion_shipping_areas');

            $table->integer('userId')->unsigned()->nullable();
            $table->foreign('userId')->references('id')
            ->on('users');

            $table->string('UID');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
